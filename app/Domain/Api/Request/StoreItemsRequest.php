<?php

namespace App\Domain\Api\Request;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class StoreItemsRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->route()->parameters());
        return [
            'name' =>'required',
        ];
    }

    public function persist()
    { 
        //dd($this->get('title'));
        return array_merge(
            //$this->only('name')
            $this->only('name'),['user_id' => Auth::user()->id]
        );  
    }
}