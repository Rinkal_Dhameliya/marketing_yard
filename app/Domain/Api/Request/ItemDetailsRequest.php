<?php

namespace App\Domain\Api\Request;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class   ItemDetailsRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->route()->parameters());
        return [
            'item_id' => 'required',
            'sub_item_id' => 'required',
            'price' =>'required',
            'date' => 'required',
            'location' => 'required',
            'quantity' => 'required',
            'loaded' => 'required',
            'commission_agent' => 'required',
            'bharti' => 'required',
        ];
    }

    public function persist()
    { 
        return array_merge(
            $this->only('item_id','sub_item_id','price','date','location','quantity','loaded','commission_agent','bharti'),['user_id' => Auth::user()->id]
        );  
    }
}