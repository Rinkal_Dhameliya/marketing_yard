<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ItemDetails extends Model
{ 
    use Notifiable;
    
    protected $table = 'item_details';

    protected $fillable = [
    	'user_id','item_id','sub_item_id','price','date','location','quantity','loaded','commission_agent','bharti'
    ];

    public function item(){
    	return $this->hasMany('App\Models\Items', 'id', 'item_id');
    }

    public function subItem(){
    	return $this->hasMany('App\Models\SubItems', 'id', 'sub_item_id');
    }

    public function loaders(){
        return $this->hasMany('App\Models\Loading');
    }

    public function itemusers(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}