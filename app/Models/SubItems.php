<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SubItems extends Model
{
    use Notifiable;

    protected $table = 'sub_items';

    protected $fillable = [
    	'item_id','user_id','name'
    ];

   /* public function subuseritems(){
    	return $this->hasMany('App\Models\Items', 'id', 'item_id');
    }*/

    public function subuseritems(){
        return $this->belongsTo(Items::class,'item_id','id');
    }

    public function itemusers(){
        return $this->belongsTo(User::class,'user_id','id');
    }

}