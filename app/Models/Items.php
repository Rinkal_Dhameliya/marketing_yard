<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Items extends Model
{
    use Notifiable;

    protected $table = 'items';

    protected $fillable = [
    	'name','user_id'
    ];

    public function subitems()
    {
        return $this->hasMany(SubItems::class,'item_id');
    }

    public function useritems(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}