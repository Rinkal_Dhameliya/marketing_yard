<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Loading extends Model
{
    use Notifiable;

    protected $table = 'loading';
    
    protected $fillable = [
    'item_details_id','date','quantity','gate_pass','car_number','driver_name'
    ];

    public function itemDetail(){
    	return $this->belongsTo('App\Models\ItemDetails', 'id', 'item_details_id');
    }

    public function userLoading(){
    	return $this->belongsTo(ItemDetails::class, 'id', 'item_user_id');
    }
}