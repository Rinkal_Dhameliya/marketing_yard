<?php 

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Api\Request\ItemDetailsRequest;
use App\Models\ItemDetails;
use App\Models\SubItems;

class ItemDetailsController extends Controller
{
 	public function store(ItemDetailsRequest $request)
 	{
 		//dd($request->persist());
 		$data = SubItems::query()->where('item_id', $request->item_id)->where('id', $request->sub_item_id)->count();

 		//dd($data);
 		
 		if($data){
    	$details = ItemDetails::create($request->persist());
    		return response()->json([
	            'success' => true,
	            'message' => 'Item-Details Added Successfully.',
	            'data' => [
                    'id' => $details->getKey(),
                ],
	        ]);
    	}
    	return response()->json([
	            'success' => false,
	            'message' => 'Something went wrong.',
	        ]);
 	}   

 	public function update(ItemDetailsRequest $request, $id)
 	{
 		$details = ItemDetails::where('id',$id)->update($request->persist());

 		if(!empty($details)){
 			return response()->json([
 				'success' => true,
 				'message' => 'Item-Details Edit Successfully.',
 			]);
 		}
 		return response()->json([
 				'success' => false,
 				'message' => 'Something went wrong.',
 			]);
 	}

 	public function destroy($id)
 	{
 		$details = ItemDetails::find($id)->delete();

 		if(!empty($details)){
 			return response()->json([
 				'success' => true,
 				'message' => 'Item-Details Delect Successfully.',
 			]);
 		}
 		return response()->json([
 				'success' => false,
 				'message' => 'Something went wrong.',
 			]);
 	}
}