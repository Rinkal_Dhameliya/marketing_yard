<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Api\Request\ItemDetailsRequest;
use App\Models\ItemDetails;
use App\Models\Loading;
use App\Http\Resources\LoadedViewDetailsCollection;
use Illuminate\Support\Facades\Auth;
//use App\Http\Resources\AgentCollection;

class LoadedViewDetailsController extends Controller
{
    public function show(Request $request)
    { 
        //$items = Loading::with(['loaded'])->groupBy('item_details_id')->get();

    //$commistion = ItemDetails::query()->has('loaders','>',0)->get();
        /*->where('user_id', Auth::user()->id)*/

        if(isset($request->date) && !empty($request->date)){
        $commistion = ItemDetails::where('user_id', Auth::user()->id)->whereHas('loaders',function ($query) use($request) { 
        return $query->whereDate('date', $request->date);
        })->where('item_details_id', $request->user_id)->get();}
        else{
            $commistion = ItemDetails::query()->where('user_id', Auth::user()->id)->has('loaders')->get();
        }

        return response()->json([
                'success' => true,
                'message' => 'Load View Details Successfully.',
                //'commission_agent' => new AgentCollection($agent)
                'Load' => new LoadedViewDetailsCollection($commistion)
            ]);
    }
}