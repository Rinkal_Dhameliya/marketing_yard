<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ItemDetails;
use App\Models\Items;
use App\Models\SubItems;
use App\Http\Resources\ItemListCollection;
use Illuminate\Support\Facades\Auth;

class ItemListController extends Controller
{
    public function show(Request $request)
    {
    	// $items = ItemDetails::with(['item','subItem','itemusers'])->where('user_id', Auth::user()->id)->groupBy('item_id')->get();

        // $items = SubItems::with(['subuseritems','itemusers'])->where('user_id', Auth::user()->id)->groupBy('item_id')->get();

        $items = Items::with(['subitems'])->where('user_id', Auth::user()->id)->get();

     
    	return response()->json([
                'success' => true,
                'message' => 'Item List View Successfully.',
                'ItemList' => new ItemListCollection($items)   
            ]);
    }
}