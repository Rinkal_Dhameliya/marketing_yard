<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Api\Request\LoginRequest;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\JsonResponse;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login(LoginRequest $request, Hasher $hash)
    {
        $credentials = $request->only('email', 'password');

        if ($token = $this->guard('api')->attempt($credentials)) {
            return $this->respondWithToken($token,$request,$hash);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'Password Is Incorrect.'
            ]
        );
    }

    protected function respondWithToken($token,$request,$hash)
    {   
        $user = User::where($request->only('email'))->first();
        if ($hash->check($request->get('password'), $user->getAuthPassword())) {

            if (!$user->is_verified) {
                return new JsonResponse([
                    'success' => false,
                    'message' => 'Verify this account. To verify your Marketing Yard account please check your inbox for an email verification.'
                ]);
            }

            return response()->json([
                'data' => [
                    'id' => $user->getKey(),
                    'email' => $user->email,
                    'name' => $user->name,
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'expires_in' => $this->guard('api')->factory()->getTTL() * 60

                ],
                'success' => true
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Incorrect Password.',
        ]);
    }

    public function guard()
    {
        return Auth::guard('api');
    }

    public function logout()
    {
    	  $this->guard('api')->logout();

        return response()->json([
            'success' => true,
            'message' => 'Successfully logged out'
        ]);
    }

}
