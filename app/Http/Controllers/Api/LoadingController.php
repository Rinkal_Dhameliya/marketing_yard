<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Loading;
use App\Models\ItemDetails;
use App\Domain\Api\Request\LoadingRequest;
use App\Http\Controllers\Api\ItemDetailsController;

class LoadingController extends Controller
{
    public function store(LoadingRequest $request)
    {
        $totalQuantity = Loading::select(\DB::raw("SUM(quantity) as total_quantity"))->where('item_details_id',$request->item_details_id)->groupBy('item_details_id')->first();

        $itemQuantity = ItemDetails::whereId($request->item_details_id)->first();
                                    
        if(isset($totalQuantity->total_quantity) && !empty($totalQuantity->total_quantity)){  
            $quantity = ItemDetails::whereId($request->item_details_id)->first();
            if(($quantity->quantity) >= ($totalQuantity->total_quantity)+($request->quantity)){
                    $loading = Loading::create($request->persist()); 
                    ItemDetails::whereId($request->item_details_id)->update([
                    'loaded' => false 
                    ]);  
            }else{
                    return response()->json([
                    'success' => false,
                    'message' => 'quantity may not be exeed.',
                ]);
            }
        }else{
            if($itemQuantity->quantity >= $request->quantity){
                $loading = Loading::create($request->persist()); 
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'quantity may not be exeed.',
                ]);
            }
        }
              
        if(!empty($loading)){
            return response()->json([
    			'success' => true,
    			'message' => 'Loading Added Successfully.',
    		]);
    	}
    	return response()->json([
    		'success' => false,
    		'message' => 'Something went wrong.',
    	]);
    }

    public function update(LoadingRequest $request, $id)
    {
        $loadingId = Loading::whereId($id)->first();

        $totalQuantity = Loading::select(\DB::raw("SUM(quantity) as total_quantity"))->where('item_details_id',$request->item_details_id)->where('id',"!=",$id)->groupBy('item_details_id')->first();

        $itemQuantity = ItemDetails::whereId($request->item_details_id)
                                ->first();
        
        if(isset($totalQuantity->total_quantity) && !empty($totalQuantity)) 
        {
            $quantity = ItemDetails::whereId($loadingId->item_details_id)->first();            

            if(($quantity->quantity) >= ($totalQuantity->total_quantity)+($request->quantity)){ 
                
                $loading = Loading::where('id',$id)->update($request->persist());
                ItemDetails::whereId($loadingId->item_details_id)->update([
                'loaded' => false 
                ]);  
            }else{
                   return response()->json([
                    'success' => false,
                    'message' => 'quantity may not be exeed.',
                ]);
            }
        }else{
         if($itemQuantity->quantity >= $request->quantity){
            $loading = Loading::where('id',$id)->update($request->persist());
        }else{   
            return response()->json([
                    'success' => false,
                    'message' => 'quantity may not be exeed.',
                ]);
            }
        }         
        if(!empty($loading)){
    		return response()->json([
    			'success' => true,
    			'message' => 'Loading Edit Successfully.',
    		]);
    	}
    	return response()->json([
    		'success' =>false,
    		'message' => 'Something went wrong.',
    	]);
    }
    public function destroy($id)
    {
    	$loading = Loading::find($id)->delete();
    	if(!empty($loading)){
    		return response()->json([
    			'success' => true,
    			'message' => 'Loading Delect Successfully.',
    		]);
    	}
        return response()->json([
            'success' => false,
            'message' => 'Something went wrong.', 
        ]);
    }
}