<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Api\Request\SubItemsRequest;
use App\Models\SubItems;
use Illuminate\Support\Facades\Auth;

class SubItemsController extends Controller
{
    public function store(SubItemsRequest $request)
    {  
    	$subitem = SubItems::create($request->persist());

    	if(!empty($subitem)){
    		return response()->json([
    			'success' => true,
    			'message' => 'Subitem Add Successfully.',
                'data' => [
                    'sub_item_id' => $subitem->getKey(),
                    'item_id' => $subitem->item_id,
                    'name' => $subitem->name,
                ],
    		]);
    	}
    	return response()->json([
    		'success' => false,
    		'message' => 'Somthing went wrong',
    	]);
    }

    public function update(SubItemsRequest $request, $id)
    {
    	$subitem = SubItems::where('id',$id)->update($request->persist());
    	if(!empty($subitem)) {
    		return response()->json([
    			'success' => true,
    			'message' => 'Subitem Edit Successfully.',
    		]);
    	}
    	return response()->json([
    		'success' => false,
    		'message' => 'Somthing went wrong.',
    	]);
    }

    public function destroy($id)
    {
    	$subitem = SubItems::find($id)->delete();

    	if(!empty($subitem)){
    		return response()->json([
    			'success' => true,
    			'message' => 'Subitem Delete Successfully.',
    		]);
    	}

    	return response()->json([
    		'success' => false,
    		'message' => 'Somthing went wrong.',
    	]);
    }
}
