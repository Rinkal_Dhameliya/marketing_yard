<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\Items; 
use App\Models\SubItems; 
use App\Http\Resources\SubItemCollection;
use App\Http\Resources\ItemCollection;

class ItemListCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /*return parent::toArray($request);*/

        return $this->collection->map(function ($itemlist) use ($request) {
        
            return [  

                    'Item_id' => $itemlist->id,
                    'Item_name' => $itemlist->name,
                    'Subitems' => new ItemCollection($itemlist->subitems)
                  //  'Item' => new ItemCollection($itemlist)
                    ];    
            });
    }
    public function with($request)   
    {
        return [
            'success' => true,
        ];
    }   
}