<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\ItemDetails; 

class DateWiseItemViewDetailsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item) use ($request) {

            return [
                    'id' => $item->getKey(),
                    'item_id' => $item->item_id,
                    'sub_item_id' => $item->sub_item_id,
                    'price' => $item->price,
                    'quantity' => $item->quantity,
                    'bharti' => $item->bharti,
                    'loaded' => $item->loaded,
                    'location' => $item->location,
                    'commission agent' => $item->commission_agent,
                ];    
                });
    }
    public function with($request)
    {
        return [
            'success' => true,
        ];
    }
}