<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\ItemDetails;
use App\Models\SubItems;
use Illuminate\Support\Facades\Auth;

class SubItemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($subitem) use ($request) {

           // $totalQuantity = ItemDetails::select (\DB::raw("SUM(quantity) as total_quantity"))->where('sub_item_id',$subitem->id)->groupBy('sub_item_id')->first();
                //dd('sub_item_id',$subitem->id);

          $subItem = SubItems::where('id',$subitem->id)->where('user_id', Auth::user()->id)->get();


                return [
                    //'subitem_id' => $subitem["subItem"][0]->id,
                    //'sub_item_name' => isset($subitem["subItem"][0]->name)?$subitem["subItem"][0]->name:'',
                  'subitem_id' => $subitem->id,
                  'name' => $subitem->name
                ];    
            });
    }
    public function with($request)
    {
        return [
            'success' => true,
        ];
    }   
}