<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\ItemDetails; 
use App\Models\Loading;

class ItemViewDetailsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return $this->collection->map(function ($item) use ($request) {
            
            $item_id = ($item->item_id);
            $sub_item_id = ($item->sub_item_id);
            $price = ($item->price);
            $quantity = ($item->quantity);
            $bharti =  ($item->bharti);
            $fixbharti = (($bharti)/20);
            $date = ($item->date);

            $total = ($price * $quantity * $fixbharti);

            $totalQuantity = Loading::select (\DB::raw("SUM(quantity) as total_quantity"))->where('item_details_id',$item->id)->groupBy('item_details_id')->first();
            
            $unloaded_quantity = 0;
            if(isset($totalQuantity->total_quantity)){
                  
                $unloaded_quantity = ($quantity - $totalQuantity->total_quantity);

                if($unloaded_quantity == 0){
                    ItemDetails::whereId($item->id)->update(['loaded' => true]);
                }
                return [
                    'id' => $item->getKey(),
                    'item_id' => $item->item_id,
                    'sub_item_id' => $item->sub_item_id,
                    'quantity' => $item->quantity,
                    'price' => $item->price,
                    'bharti' => $item->bharti,
                    'unloaded_quantity' => $unloaded_quantity,
                    'loaded' => $item->loaded,
                    'total' => $total
                ];    
                }else{
                    return [
                    'id' => $item->getKey(),
                    'item_id' => $item->item_id,
                    'sub_item_id' => $item->sub_item_id,
                    'quantity' => $item->quantity,
                    'price' => $item->price,
                    'bharti' => $item->bharti,
                    'unloaded_quantity' => $unloaded_quantity,
                    'loaded' => $item->loaded,
                    'total' => $total
                    ];
                }               
            });
        }
    public function with($request)
    {
        return [
            'success' => true,
        ];
    }
}