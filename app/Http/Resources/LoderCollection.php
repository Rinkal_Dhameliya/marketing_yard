<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\Loading; 

class LoderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item) use ($request) {

           // $comm = Loading::with('loaded')->where('item_details_id',$item->id)->get();

            return [
                    //'commission_agent' => $item->commission_agent,
                    'id' => $item->getKey(),
                    'item_details_id' => $item->item_details_id,
                    'quantity' => $item->quantity,
                    'gate pass' => $item->gate_pass,
                    'car number' => $item->car_number,
                    'driver name' => $item->driver_name,
                ];    
            });
    }
}
