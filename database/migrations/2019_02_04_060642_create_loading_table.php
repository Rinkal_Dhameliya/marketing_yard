<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loading', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('item_details_id');
            $table->foreign('item_details_id')->references('id')->on('item_details')->onUpdate('cascade')->onDelete('cascade');
            $table->date('date');
            $table->double('quantity');
            $table->string('gate_pass');
            $table->string('car_number');
            $table->string('driver_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loading');
    }
}
