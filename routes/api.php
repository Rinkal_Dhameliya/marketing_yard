<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function() {

	Route::post('register',  'Auth\RegisterController@register');
	Route::post('login', 'Auth\LoginController@login');
	
});

Route::group(['prefix' => 'v1', 'namespace' => 'Api','middleware' => ['jwt-auth']], function() {

	Route::get('logout', 'Auth\LoginController@logout');

	Route::resource('items','StoreItemsController');
	Route::post('item-list','ItemListController@show');

	Route::resource('sub-items','SubItemsController');

	Route::resource('item-details','ItemDetailsController');
	Route::post('item-view-details', 'ItemViewDetailsController@show');
	Route::post('item-date-wise-details', 'DateWiseItemViewDetailsController@show');

	Route::resource('loading', 'LoadingController');
	Route::post('loaded-view-details', 'LoadedViewDetailsController@show');

});